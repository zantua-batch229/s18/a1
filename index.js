// 3
function addTwoNumbers(num1, num2){
	let additionResult = num1+num2;
	console.log("Displayed sum of " + num1 + " and " + num2 + "\n" + additionResult);

}

// 4
function subtractTwoNumbers(num1, num2){
	let subtractionResult = num1-num2;
	console.log("Displayed difference of " + num1 + " and " + num2 + "\n" + subtractionResult);

}
// 5
addTwoNumbers(5,15);
// 6
subtractTwoNumbers(20,5);

// 7
function multiplyTwoNumbers(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;

}

// 8
function divideTwoNumbers(num1, num2){
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;

}

// 9
let product = multiplyTwoNumbers(50,10);
console.log(product);
// 10
let quotient = divideTwoNumbers(50, 10);
console.log(quotient);



// 11
function getAreaofCircle(radius){
	let area = 3.1416 * radius ** 2;
	console.log("The result of getting the area of a circle with " + radius + ":");
	return area;

}
// 12
let circleArea = getAreaofCircle(15);
console.log(circleArea);

// 13
function averageOfFourNumbers(num1, num2, num3, num4){
	console.log("the average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":")
	return (num1+num2+num3+num4)/4
}

// 14
let averageVar = averageOfFourNumbers(20,40,60,80);
console.log(averageVar);

// 15
function checkIfPassed(score, totalScore){
	let percentage = (score/totalScore) * 100;
	let ifPassed = percentage>75;
	console.log("Is " + score + "/" + totalScore + " a passing score?")
	return ifPassed;
}
// 16
let isPassingScore = checkIfPassed(38, 50);
console.log(isPassingScore);




